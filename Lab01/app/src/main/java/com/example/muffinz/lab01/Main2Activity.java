package com.example.muffinz.lab01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView T2;
    TextView T3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        T2 = findViewById(R.id.textView);
        String T1;
        T1 = getIntent().getStringExtra("T1");
        T2.setText("Hello " + T1);

        T3 = findViewById(R.id.textView2);
    }


    /**
     * Called when the user presses the button
     * Don't mind the name, the function was copy pasted from something unrelated
     * @param view
     */
    public void sendMessage(View view) {
        Intent intent = new Intent(this, Main3Activity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        String T4;
        T4 = data.getStringExtra("T4");
        T3.setText("From A3: " + T4);
    }
}
