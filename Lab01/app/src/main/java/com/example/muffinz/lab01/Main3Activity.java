package com.example.muffinz.lab01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Main3Activity extends AppCompatActivity {

    EditText T4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        T4 = findViewById(R.id.T4);
    }

    /**
     * Called when the user presses the button
     * Don't mind the name, the function was copy pasted from something unrelated
     * @param view
     */
    public void sendMessage(View view) {
        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra("T4", T4.getText().toString());
        setResult(1, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra("T4", "");
        setResult(1, intent);
        finish();
    }
}
