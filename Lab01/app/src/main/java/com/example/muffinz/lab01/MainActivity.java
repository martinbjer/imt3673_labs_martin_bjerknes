package com.example.muffinz.lab01;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    private EditText T1;
    private Spinner L1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        T1 = findViewById(R.id.editText);
        L1 = findViewById(R.id.spinner);
        loadSetting();

        if( getIntent().getBooleanExtra("EXIT", false)){
            finish();
            return; // add this to prevent from doing unnecessary stuffs
        }
    }


    /**
     * Called when the user presses the button
     * Don't mind the name, the function was copy pasted from something unrelated
     * @param view
     */
    public void sendMessage(View view) {
        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra("T1", T1.getText().toString());
        startActivity(intent);
        saveSetting();
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        startActivity(intent);
        saveSetting();
        finish();
    }

    private void saveSetting() {
        SharedPreferences sharedPrefs = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putInt("itemIndex", (int)L1.getSelectedItemId());
        editor.commit();

    }

    protected void loadSetting(){
        SharedPreferences sharedPrefs = getPreferences(Context.MODE_PRIVATE);
        int value = sharedPrefs.getInt("itemIndex", 0);

        L1.setSelection(value);
    }
}
