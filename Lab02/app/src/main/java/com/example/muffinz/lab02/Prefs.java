package com.example.muffinz.lab02;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Activity for the preferences menu
 */
public class Prefs extends AppCompatActivity {

    EditText url; //URL for RSS or Atom feed
    EditText limit; //Limit on number of RSS items to store
    EditText frequency; //The frequency in minutes of feed fetching

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prefs);

        url = findViewById(R.id.url);
        limit = findViewById(R.id.itemLimit);
        frequency = findViewById(R.id.frequency);

        final Activity activity = this;

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity);
        url.setText(sharedPrefs.getString("URL", "https://www.vg.no/rss/feed/forsiden/"));
        limit.setText(Integer.toString(sharedPrefs.getInt("LIMIT", 60)));
        frequency.setText(Integer.toString(sharedPrefs.getInt("FREQUENCY", 1)));


        url.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b){
                    SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity);
                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString("URL", url.getText().toString());
                    editor.commit();
                    RSSFetcher.setSettings();
                }
            }
        });

        limit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b){
                    if (!limit.getText().toString().isEmpty()) {
                        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity);
                        SharedPreferences.Editor editor = sharedPrefs.edit();
                        editor.putInt("LIMIT", Integer.parseInt(limit.getText().toString()));
                        editor.commit();
                        RSSFetcher.setSettings();
                    }
                }
            }
        });

        frequency.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b){
                    if (!frequency.getText().toString().isEmpty()) {
                        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(activity);
                        SharedPreferences.Editor editor = sharedPrefs.edit();
                        editor.putInt("FREQUENCY", (Integer.parseInt(frequency.getText().toString())));
                        editor.commit();
                        RSSFetcher.setSettings();
                    }
                }
            }
        });
    }
}
