package com.example.muffinz.lab02;

/**
 * Simple helper class for representing an item in a RSS or Atom feed
 */
public class RssFeedModel {
    public String title;
    public String link;

    public RssFeedModel(String title, String link) {
        this.title = title;
        this.link = link;
    }
}