package com.example.muffinz.lab02;

import android.app.Service;
import android.content.ClipData;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

/**
 * Service responsible for fetching RSS/Atom feed
 */
public class RSSFetcher extends Service {
    private static boolean isRunning = false;
    private static boolean newItems = false;
    private static ArrayList<RssFeedModel> rssItems;
    private static String rssFeed;
    private static int frequency;
    private static int limit;
    private static RSSFetcher This;
    private static long milliseconds;

    private static final String Tag = "RSSFetcher";

    @Override
    public void onCreate(){
        super.onCreate();
        rssItems = new ArrayList<>();
        This = this;
        setSettings();
        isRunning = true;
        new Thread(new FetchThread()).start();

        Toast.makeText(RSSFetcher.this,
                "RSSFetcher Started",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        isRunning = false;
        milliseconds = 0;
    }

    @Override
    public IBinder onBind(Intent intent) {
       return null;
    }

    /**
     * Applies the settings that are set from the preferences menu
     */
    public static void setSettings(){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(This);
        rssFeed = sharedPrefs.getString("URL", "https://www.vg.no/rss/feed/forsiden/");
        frequency = sharedPrefs.getInt("FREQUENCY", 1);
        limit = sharedPrefs.getInt("LIMIT", 60);
        milliseconds = 0;
    }

    /**
     * Returns the alive state of this service
     * @return boolean isAlive
     */
    public static boolean isAlive(){
        return isRunning;
    }

    /**
     * Returns true if there are new items in the feed, false otherwise
     * @return boolean updated
     */
    public static boolean updated(){
        return newItems;
    }

    /**
     * Adds the items in the feed to the supplied ArrayList in a thread safe manner
     * @param other list to insert feed items into
     * @return boolean success flag
     */
    public static boolean getRssFeed(ArrayList<RssFeedModel> other){
        other.clear();
        if (rssItems != null) {
            synchronized (rssItems) {
                for (RssFeedModel item : rssItems) {
                    other.add(item);
                }
                newItems = false;
            }
            return true;
        }
        return false;
    }

    /**
     * Thread for fetching the feed
     */
    private class FetchThread implements Runnable {
        public void run(){
            while(isRunning) {
                try {
                    fetch();
                    while (milliseconds < frequency * 1000 * 60) {
                        Thread.sleep(500);
                        milliseconds += 500;
                        if (milliseconds % 1000 == 0) {
                            Log.d(Tag, Long.toString(frequency * 60 - milliseconds / 1000) + " seconds until refresh");
                        }
                    }
                    milliseconds = 0;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Function for fetching the RSS/Atom feed
     */
    private void fetch(){
        try {
            URL url = new URL(rssFeed);
            InputStream inputStream = url.openConnection().getInputStream();
            parseFeed(inputStream);
        } catch (IOException e) {
            Log.e(Tag, "Error", e);
        } catch (XmlPullParserException e) {
            Log.e(Tag, "Error", e);
        }
    }

    /**
     * function that parses an RSS or Atom feed
     * Main source: https://www.androidauthority.com/simple-rss-reader-full-tutorial-733245/
     * @param inputStream
     * @throws XmlPullParserException
     * @throws IOException
     */
    private void parseFeed(InputStream inputStream) throws XmlPullParserException, IOException {
        String title = null;
        String link = null;
        boolean isItem = false;

        try {
            XmlPullParser xmlPullParser = Xml.newPullParser();
            xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            xmlPullParser.setInput(inputStream, null);

            xmlPullParser.nextTag();
            synchronized (rssItems) {
                while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {
                    int eventType = xmlPullParser.getEventType();

                    String name = xmlPullParser.getName();
                    if (name == null)
                        continue;

                    if (eventType == XmlPullParser.END_TAG) {
                        if (name.equalsIgnoreCase("item") || name.equalsIgnoreCase("entry")) {
                            isItem = false;
                        }
                        continue;
                    }

                    if (eventType == XmlPullParser.START_TAG) {
                        if (name.equalsIgnoreCase("item") || name.equalsIgnoreCase("entry")) {
                            isItem = true;
                            continue;
                        }
                    }

                    String result = "";
                    if (xmlPullParser.next() == XmlPullParser.TEXT) {
                        result = xmlPullParser.getText();
                        xmlPullParser.nextTag();
                    }

                    if (name.equalsIgnoreCase("title") && isItem) {
                        Log.d(Tag, "Parsing text ==> " + result);
                        title = result;
                    } else if ((name.equalsIgnoreCase("link") || name.equalsIgnoreCase("id")) && isItem) {
                        Log.d(Tag, "Parsing text ==> " + result);
                        link = result;
                    }

                    if (title != null && link != null && !link.isEmpty()) {
                        if (isItem) {
                            boolean newItem = true;
                            for (RssFeedModel item : rssItems){
                                if (item.link.contentEquals(link)){
                                    newItem = false;
                                }
                            }
                            if (newItem) {
                                newItems = true;
                                RssFeedModel item = new RssFeedModel(title, link);
                                rssItems.add(0, item);
                            }
                        }

                        title = null;
                        link = null;
                        isItem = false;
                    }
                }

                while(rssItems.size() > limit){
                    rssItems.remove(rssItems.size() - 1);
                }
            }
        } finally {
            inputStream.close();
        }
    }
}
