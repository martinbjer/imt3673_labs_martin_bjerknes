package com.example.muffinz.lab02;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import android.app.ListActivity;
import android.widget.Toast;


/**
 * Main activity for the application
 */
public class MainActivity extends ListActivity {
    ArrayList<String> listItems = new ArrayList<String>();
    ArrayAdapter<String> adapter;
    ArrayList<RssFeedModel> feed = new ArrayList<RssFeedModel>();
    private boolean running = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listItems);
        setListAdapter(adapter);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(feed.get(position).link));
                    startActivity(intent);
                }
            }
        );

        if(!RSSFetcher.isAlive()){
            Intent intent = new Intent(this, RSSFetcher.class);
            startService(intent);
        } else {
            Toast.makeText(MainActivity.this,
                    "RSSFetcher already running",
                    Toast.LENGTH_LONG).show();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                while(running){
                    try {
                        Log.d("MainActivity", "Going to sleep!");
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (RSSFetcher.updated()){
                        Log.d("MainActivity", "Getting new items!");
                        refresh();
                    }
                }
                Log.d("MainActivity", "Refresh thread stopped!");
            }
        }).start();
    }

    @Override
    protected void onStop(){
        super.onStop();
        running = false; //To stop the fetching thread
    }

    /**
     * Called when the prefs button is pressed
     * @param view
     */
    public void OnPrefs(View view){
        Intent intent = new Intent(this, Prefs.class);
        startActivity(intent);
    }

    /**
     * Function that fetches feed itmes from service
     */
    public void refresh(){
        new FetchFeedTask().execute((Void) null);
    }

    /**
     * Task for fetching feed items from the service
     */
    private class FetchFeedTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            Boolean success = false;
            while (!success){
                success = RSSFetcher.getRssFeed(feed);
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            listItems.clear();
            for (RssFeedModel item : feed){
                listItems.add(item.title);
            }
            return success;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success) {
                Toast.makeText(MainActivity.this,
                        "Feed size: " + Integer.toString(feed.size()),
                        Toast.LENGTH_LONG).show();
                adapter.notifyDataSetChanged();
            } else {
                Toast.makeText(MainActivity.this,
                        "Failed to get RSSFeed",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}
