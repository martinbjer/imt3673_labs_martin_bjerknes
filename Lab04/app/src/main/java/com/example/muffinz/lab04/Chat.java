package com.example.muffinz.lab04;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;


/**
 * Fragment for global chat
 */
public class Chat extends Fragment {
    private static final String TAG = Chat.class.getName();

    private DatabaseReference database;

    private String userName;

    private ListView chatFeed;
    private ArrayList<String> listItems = new ArrayList<>();
    private ArrayAdapter<String> adapter;

    EditText sayField;

    public Chat() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Chat.
     */
    public static Chat newInstance(String userName) {
        Chat chat = new Chat();

        Bundle args = new Bundle();
        args.putString("username", userName);
        chat.setArguments(args);
        return chat;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        database = FirebaseDatabase.getInstance().getReference();
        setUpDataBaseListeners();

        userName = getArguments().getString("username");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);

        chatFeed = view.findViewById(R.id.lv_chat);

        adapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_list_item_1, listItems);
        chatFeed.setAdapter(adapter);

        sayField = view.findViewById(R.id.txt_message);

        view.findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage(sayField.getText().toString());
            }
        });

        return view;
    }

    /**
     * Sets up listeners for database changes.
     */
    private void setUpDataBaseListeners(){
        database.child("messages").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Message msg = dataSnapshot.getValue(Message.class);
                if (msg == null){
                    Log.w(TAG, "Received message was null!");
                } else {
                    listItems.add(msg.toString());

                    adapter.notifyDataSetChanged();
                    chatFeed.setSelection(adapter.getCount() - 1);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Sends message to global chat
     * @param message
     */
    private void sendMessage(String message){
        if (message.isEmpty()){
            return;
        }

        long timestamp = System.currentTimeMillis();
        String messageText = sayField.getText().toString();

        Message msg = new Message(timestamp, userName, messageText);
        DatabaseReference messageRef = database.child("messages").push();
        messageRef.setValue(msg);

        database.child("userMessageIndex").child(userName).push().setValue(new MessageIndex(messageRef.getKey()));
        sayField.setText("");
    }

}
