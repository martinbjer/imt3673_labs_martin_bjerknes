package com.example.muffinz.lab04;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Class representing a message
 */
public class Message {
    public long d;
    public String u;
    public String m;

    /**
     * Empty constructor
     */
    public Message(){

    }

    /**
     *Constructs a message
     *
     * @param d: long Timestamp
     * @param u: String username
     * @param m: String message
     */
    public Message(long d, String u, String m){
        this.d = d;
        this.u = u;
        this.m = m;
    }

    public String toString(){
        Timestamp stamp = new Timestamp(d);
        SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm:ss", Locale.getDefault());
        return String.format("%s (%s):\n%s", u, date.format(d), m);
    }
}
