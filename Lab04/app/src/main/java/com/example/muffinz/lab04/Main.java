package com.example.muffinz.lab04;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Main activity for app, hosts the chat and friend list fragments
 */
public class Main extends ChatActivity {
    private static final String TAG = Main.class.getName();
    private static final int requestCodeUserName = 0;

    private static DatabaseReference database;
    private static FirebaseAuth auth;

    private String userName;

    private SectionsPagerAdapter sectionsPagerAdapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        final Context context = this;
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        viewPager = findViewById(R.id.container);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_message);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_friends);

        ImageButton prefsButton = findViewById(R.id.btn_prefs);
        prefsButton.setOnTouchListener(new ImageButtonTouchListener());
        prefsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Prefs.class);
                startActivity(intent);
            }
        });

        viewPager.setCurrentItem(0);

        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference();

        auth.signInAnonymously().addOnCompleteListener(this,
                new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInAnonymously:success");;
                    Toast.makeText(context,
                            "User logged in!",
                            Toast.LENGTH_LONG
                    ).show();
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInAnonymously:failure", task.getException());
                    Toast.makeText(context,
                            "User NOT logged in!",
                            Toast.LENGTH_LONG
                    ).show();
                }
            }
        });
        getUserName();

        if (!NotificationService.IsRunning()){
            startService(new Intent(this, NotificationService.class));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == requestCodeUserName){
            if (data == null){
                Log.d(TAG, "onActivityResult, intent is null!");
            }
            userName = data.getStringExtra("username");
            database.child("users").push().setValue(new User(userName));
        }
    }

    /**
     * FragmentPagerAdapter for the two fragments that this activity hosts (Chat and friend list)
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return Chat.newInstance(userName);
                case 1:
                    return FriendList.newInstance();
            }
            Log.d(TAG, "getItem: " + position + " returned null!");
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position){
            return "";
        }
    }

    /**
     * Gets the username from preferences or ask the user to provide one if none exists
     */
    private void getUserName(){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        userName = sharedPrefs.getString("username", "");
        if (userName.isEmpty()) {
            //Send user to name choosing activity
            Intent intent = new Intent(this, NameChooser.class);
            startActivityForResult(intent, requestCodeUserName);
        }
    }
}
