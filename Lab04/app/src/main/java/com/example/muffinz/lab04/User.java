package com.example.muffinz.lab04;

/**
 * Class representing a user node in real time database
 */
public class User {
    public String u;

    public User(){

    }

    /**
     *
     * @param u, username
     */
    public User(String u){
        this.u = u;
    }
}
