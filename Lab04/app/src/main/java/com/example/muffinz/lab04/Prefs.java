package com.example.muffinz.lab04;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.HashMap;

/**
 * Activity for user preferences
 */
public class Prefs extends ChatActivity {
    private static final String TAG = Prefs.class.getName();

    private SharedPreferences sharedPrefs;

    private final HashMap<String, Integer> spinnerOptions = new HashMap<>();
    private ArrayAdapter<String> spinnerAdapter;

    private Spinner notiFreq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prefs);

        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        String[] keys = new String[] {"1s", "10s", "30s", "1m", "10m", "30m"};
        spinnerOptions.put(keys[0], 1);
        spinnerOptions.put(keys[1], 10);
        spinnerOptions.put(keys[2], 30);
        spinnerOptions.put(keys[3], 60);
        spinnerOptions.put(keys[4], 10 * 60);
        spinnerOptions.put(keys[5], 30 * 60);

        spinnerAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                keys
        );

        notiFreq = findViewById(R.id.s_notifiaction_freq);
        notiFreq.setAdapter(spinnerAdapter);

        int selectedOption = sharedPrefs.getInt("selectedOption", 0);
        notiFreq.setSelection(selectedOption);
    }

    @Override
    public void onBackPressed(){
        savePrefs();
        super.onBackPressed();
    }

    /**
     * Called when the apply button is clicked
     *
     * @param view
     */
    public void OnApply(View view){
       onBackPressed();
    }

    /**
     * Saves the preferences to shared preferences
     */
    private void savePrefs(){
        SharedPreferences.Editor editor = sharedPrefs.edit();
        int notificationFreq = spinnerOptions.get(notiFreq.getSelectedItem().toString());

        editor.putInt("selectedOption", notiFreq.getSelectedItemPosition());
        editor.putInt("notificationFreq", notificationFreq);
        editor.apply();
    }
}
