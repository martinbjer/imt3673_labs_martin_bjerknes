package com.example.muffinz.lab04;

/**
 * Class representing a message index node in database
 */
public class MessageIndex {
    public String i;

    public MessageIndex(){
        //Required empty constructor
    }

    /**
     *
     * @param i, message index
     */
    public MessageIndex(String i){
        this.i = i;
    }
}
