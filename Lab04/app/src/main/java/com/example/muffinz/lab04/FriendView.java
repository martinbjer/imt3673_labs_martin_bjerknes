package com.example.muffinz.lab04;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Activity that displays all current and new messages from a user
 */
public class FriendView extends ChatActivity {
    private static final String TAG = Chat.class.getName();

    private DatabaseReference database;
    private String username;

    private ListView chatFeed;
    private ArrayList<String> listItems = new ArrayList<>();
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_view);
        username = getIntent().getStringExtra("username");

        database = FirebaseDatabase.getInstance().getReference();
        setUpDataBaseListeners();

        chatFeed = findViewById(R.id.lv_friend_messages);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listItems);
        chatFeed.setAdapter(adapter);

        TextView label = findViewById(R.id.txt_friend_view_label);
        label.setText(String.format("Viewing messages by %s", username));
    }

    /**
     * Sets up listeners for database changes.
     */
    private void setUpDataBaseListeners(){
        database.child("userMessageIndex").child(username).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                MessageIndex msgIndex = dataSnapshot.getValue(MessageIndex.class);
                Log.d(TAG, "Index: " + msgIndex.i);
                if (msgIndex == null){
                    Log.d(TAG, "Received message was null!");
                } else {
                    database.child("messages").child(msgIndex.i).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Message msg = dataSnapshot.getValue(Message.class);
                            if (msg == null){
                                Log.d(TAG, "Received message was null!");
                            } else {
                                listItems.add(msg.toString());
                                adapter.notifyDataSetChanged();
                                chatFeed.setSelection(adapter.getCount() - 1);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
