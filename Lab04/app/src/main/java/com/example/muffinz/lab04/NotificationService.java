package com.example.muffinz.lab04;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Service that notifies the user of new messages in chat
 */
public class NotificationService extends Service {
    private static final String TAG = NotificationService.class.getName();
    private static boolean isRunning = false;

    private DatabaseReference database;
    private SharedPreferences sharedPrefs;
    private Context context;

    private NotificationManager notificationManager;
    private int notificationFreq;
    private String CHANNEL_ID;

    private Timer timer;
    private boolean notificationRaised = false;
    private static long lastNotificationTime;
    private Notification notification;

    SharedPreferences.OnSharedPreferenceChangeListener prefsListener;

    @Override
    public void onCreate(){
        super.onCreate();
        isRunning = true;

        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        database = FirebaseDatabase.getInstance().getReference();
        setUpDataBaseListeners();
        context = this;

        timer = new Timer();
        lastNotificationTime = System.currentTimeMillis();
        notificationFreq = sharedPrefs.getInt("notificationFreq", 1);
        setupNotificationChannel();
        notification = buildNotification();

        prefsListener =  new SharedPreferences.OnSharedPreferenceChangeListener(){
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if (key == "notificationFreq"){
                    notificationFreq = sharedPreferences.getInt("notificationFreq", 1);
                }
            }
        };
        sharedPrefs.registerOnSharedPreferenceChangeListener(prefsListener);

        Log.d(TAG, "Starting notification service!");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, START_STICKY_COMPATIBILITY, startId);
        Log.d("SERVICE","Started");
        return START_STICKY_COMPATIBILITY;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        isRunning = false;
        timer.cancel();
        sharedPrefs.unregisterOnSharedPreferenceChangeListener(prefsListener);
    }

    @Override
    public IBinder onBind(Intent intent){
        return null;
    }

    /**
     * Called when the main app goes out of focus
     */
    public static void onAppOutOfFocus(){
        lastNotificationTime = System.currentTimeMillis();
    }

    /**
     * Used to check if the service is running
     * @return boolean isRunning
     */
    public static boolean IsRunning(){
        return isRunning;
    }

    /**
     * Sets up listeners for database changes.
     */
    private void setUpDataBaseListeners(){
        database.child("messages").limitToLast(1).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (!ChatActivity.isInForeground()) { //Raise a notification
                    Message msg = dataSnapshot.getValue(Message.class);
                    tryRaiseNotification(msg);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Tries to raise a notification.
     * This function enforces the notification frequency.
     */
    private void tryRaiseNotification(Message msg){
        if (notificationRaised || msg.d < lastNotificationTime){
            return;
        }

        notificationRaised = true;
        long timeSinceLastNotification = System.currentTimeMillis() - lastNotificationTime;
        long delay = notificationFreq * 1000 - timeSinceLastNotification;
        if (delay < 0){
            delay = 0;
        }
        Log.i(TAG, "Raising notification in: " + delay / 1000 + " Seconds");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                notificationManager.notify(0, notification);
                notificationRaised = false;
                lastNotificationTime = System.currentTimeMillis();
            }
        }, delay);
    }

    /**
     * Builds a message notification
     * @return Notification
     */
    private Notification buildNotification(){
        Intent intent = new Intent(context, Main.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle("New message!")
                .setPriority(Notification.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        return builder.build();
    }

    /**
     * Sets up the notification channel
     */
    private void setupNotificationChannel(){
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            CharSequence name = "Message Notifications";
            String description = "Notifications for messages received by database";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(description);
            // Register the channel with the system
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        } else {
            CHANNEL_ID = "Not Set";
        }
    }
}
