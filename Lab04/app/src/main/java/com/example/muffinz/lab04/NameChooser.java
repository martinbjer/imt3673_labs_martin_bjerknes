package com.example.muffinz.lab04;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.Locale;
import java.util.Random;

/**
 * Activity for choosing a name
 */
public class NameChooser extends ChatActivity {
    private static final String TAG = NameChooser.class.getName();

    private static final char[] consonantArray = new char[] {
            'b', 'c', 'd', 'f', 'g', 'h', 'j',
            'k', 'l', 'm', 'n', 'p', 'q', 'r',
            's', 't', 'v', 'w', 'x', 'z'
    };
    private static final char[] vowelArray = new char[]{
            'a', 'e', 'i', 'o', 'u', 'y'
    };

    private EditText nameInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_chooser);

        nameInput = findViewById(R.id.text_nameInput);
        nameInput.setText(generateName());
    }

    /**
     * Called when the apply button is clicked
     *
     * Saves username to preferences
     * @param view
     */
    public void OnApply(View view){
        onBackPressed();
    }

    @Override
    public void onBackPressed(){
        Intent intent = getIntent();
        intent.putExtra("username", saveAndGetUserName());
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    /**
     * Saves and returns the username currently input to the activity EditText
     * @return String username
     */
    private String saveAndGetUserName(){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        String userName = nameInput.getText().toString();
        editor.putString("username", userName);
        editor.apply();
        return userName;
    }

    /**
     * Generates a name, based on consonant/vowel distribution
     * @return string name
     */
    private String generateName(){
        Random rng = new Random();
        int nameLength = rng.nextInt(7) + 3;
        String name = "";
        int consonants = nameLength / 2 + rng.nextInt (3) - 1;
        int vowels = nameLength - consonants;
        int cCount = 0;
        for (int i = 0; i < nameLength; i++) {
            if (((rng.nextFloat() >= 0.5f && consonants > 0) || vowels < 1) && cCount != 2) {
                name += consonantArray [ rng.nextInt(consonantArray.length)];
                consonants--;
                cCount++;
            } else if (vowels > 0 || cCount == 2) {
                name += vowelArray [rng.nextInt(vowelArray.length)];
                vowels--;
                cCount = 0;
            }
            if (i == 0){
                name = name.toUpperCase(Locale.getDefault());
            }
        }
        return name;
    }
}
