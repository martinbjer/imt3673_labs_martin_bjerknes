package com.example.muffinz.lab04;

import android.support.v7.app.AppCompatActivity;

/**
 * Base class for chat activities.
 * Helps application determine if the app is in the foreground or not
 */
public abstract class ChatActivity extends AppCompatActivity {
    private static int foregroundCounter = 0;

    @Override
    public void onResume(){
        super.onResume();
        foregroundCounter++;
    }

    @Override
    public void onStop(){
        super.onStop();
        foregroundCounter--;
        NotificationService.onAppOutOfFocus();
    }

    /**
     * Determines if the activity is in foreground
     * @return boolean inForeground
     */
    public static boolean isInForeground(){
        return foregroundCounter > 0;
    }
}
