package com.example.muffinz.lab04;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Fragment for friend list
 */
public class FriendList extends Fragment {
    private static final String TAG = FriendList.class.getName();

    private DatabaseReference database;

    private ListView friendList;
    private ArrayList<String> listItems = new ArrayList<>();
    private ArrayAdapter<String> adapter;

    private HashSet<String> duplicateChecker = new HashSet<>();

    public FriendList() {
        // Required empty public constructor
    }

    /**
     * Call this to get an instance of the fragment
     * @return FriendList fragment
     */
    public static FriendList newInstance() {
        FriendList fragment = new FriendList();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = FirebaseDatabase.getInstance().getReference();
        setUpDataBaseListeners();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_friend_list, container, false);

        friendList = view.findViewById(R.id.lv_friends);
        adapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_list_item_1, listItems);
        friendList.setAdapter(adapter);

        friendList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String userName = listItems.get(position);
                Intent intent = new Intent(view.getContext(), FriendView.class);
                intent.putExtra("username", userName);
                startActivity(intent);
            }
        });
        return view;
    }


    /**
     * Sets up listeners for database changes.
     */
    private void setUpDataBaseListeners(){
        database.child("users").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                User user = dataSnapshot.getValue(User.class);
                Log.d(TAG, user.u);
                if (!duplicateChecker.contains(user.u)){
                    duplicateChecker.add(user.u);
                    listItems.add(user.u);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
