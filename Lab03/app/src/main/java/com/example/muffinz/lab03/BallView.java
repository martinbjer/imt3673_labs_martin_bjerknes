package com.example.muffinz.lab03;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.Vector;

/**
 * View for gravity ball.
 */
public class BallView extends View {
    private SensorManager sensorManager;
    private Sensor accelerometer ;
    private BallSensorEventListener eventListener;
    private Vibrator vibrator;
    private ToneGenerator toneGenerator;

    final float gap = 10; //Gap for boundry
    private RectF boundry;
    private Paint boundryPaint;

    private float ballRadius;
    private Paint ballPaint;
    private Vector2 position;
    private Vector2 velocity = new Vector2(0, 0);

    //Ball physics variables
    final float delta = 0.1f; //Used for acceleration calculations
    final float drag = 0.95f; //Used for slowing down ball when hitting wall

    boolean started = false;

    /**
     * Calls the super constructor
     * @param context
     */
    public BallView(Context context) {
        super(context);
    }

    /**
     * Calls the super constructor
     * @param context
     * @param attrs
     */
    public BallView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Calls the super constructor
     * @param context
     * @param attrs
     * @param defStyle
     */
    public BallView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Initializes the BallView
     * @param context, the context of the caller
     */
    public void init(Context context){
        vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
        toneGenerator = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);

        sensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (accelerometer  == null){
            MainActivity.debugToast("Accelerometer sensor is null!", Toast.LENGTH_SHORT);
        } else {
            eventListener = new BallSensorEventListener();
        }
    }

    /**
     * registers sensor that controls the ball
     */
    public void registerSensors(){
        sensorManager.registerListener(eventListener, accelerometer , SensorManager.SENSOR_DELAY_GAME);
    }

    /**
     * Unregisters sensors to save battery life when application is not in focus
     */
    public void unregisterSensors(){
        sensorManager.unregisterListener(eventListener);
    }

    /**
     * Class for handling sensor events that control the ball
     */
    private class BallSensorEventListener implements SensorEventListener {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            physics(sensorEvent.values);
            invalidate();
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    }

    /**
     * Does the physics calculations for the ball, velocity, acceleration and collisions
     * @param accelData The sensor data from the accelerometer
     */
    private void physics(final float[] accelData){
        if (!started){
            return;
        }

        //zFactor helps reduce acceleration when the phone is put flat on a table
        float zFactor = 1 - (Math.abs(accelData[2]) / (Math.abs(accelData[0]) + Math.abs(accelData[1]) + Math.abs(accelData[2])));
        velocity = Vector2.add(velocity, new Vector2(accelData[1] * delta * zFactor, accelData[0] * delta * zFactor));
        Vector2 oldPos = position;
        position = Vector2.add(position, velocity);

        boolean collision = false;
        if (position.x - ballRadius < boundry.left || position.x + ballRadius > boundry.right) {
            velocity.x = -velocity.x * drag;
            position.x = oldPos.x;
            collision = true;
        }
        if (position.y - ballRadius < boundry.top || position.y + ballRadius > boundry.bottom) {
            velocity.y = -velocity.y * drag;
            position.y = oldPos.y;
            collision = true;
        }

        if (collision){
            giveFeedback();
        }
    }

    /**
     * Gives feedback to the user in the form of vibrations and a beep
     */
    private void giveFeedback(){
        vibrator.vibrate(100); //Using deprecated function because my phone has API level 22
        toneGenerator.startTone(ToneGenerator.TONE_PROP_BEEP);
    }

    /**
     * Function that initializes the drawing logic on the first draw.
     * Made this function because the rendering systems needs to be initialized before i can call
     * getWidth() and getHeight()
     */
    private void onFirstDraw(){
        final float w = getWidth();
        final float h = getHeight();
        position = new Vector2(w/2f, h/2f);
        ballRadius = w * 0.025f;
        boundry = new RectF(gap, gap, w - gap, h - gap);

        ballPaint = new Paint();
        ballPaint.setStyle(Paint.Style.FILL);
        ballPaint.setColor(Color.BLUE);

        boundryPaint = new Paint();
        boundryPaint.setStyle(Paint.Style.STROKE);
        boundryPaint.setStrokeWidth(10);
        boundryPaint.setColor(Color.BLACK);
    }

    /**
     * Function for drawing the ball and bounding rectangle
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!started){
            onFirstDraw();
            started = true;
        }

        canvas.drawCircle(position.x, position.y, ballRadius, ballPaint);
        canvas.drawRect(boundry, boundryPaint);
    }
}
