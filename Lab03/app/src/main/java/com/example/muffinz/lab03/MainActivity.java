package com.example.muffinz.lab03;

import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static MainActivity activity;
    private BallView ballView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set window fullscreen and remove title bar, and force landscape orientation
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_main);
        activity = this;

        ballView = findViewById(R.id.BallView);
        ballView.init(this);
        ballView.invalidate();
    }

    @Override
    protected void onResume(){
        super.onResume();
        ballView.registerSensors();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ballView.unregisterSensors();
    }

    /**
     * Function to do debugging with toasts
     * @param message Message to toast
     * @param length Duration of toast
     */
    public static void debugToast(String message, int length){
        Toast.makeText(activity, message, length).show();
    }
}
